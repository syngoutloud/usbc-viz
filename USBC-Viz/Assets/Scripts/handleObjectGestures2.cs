﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class handleObjectGestures2 : MonoBehaviour
{

    public GameObject roomCenter;
    float distanceFromCenter;
    float roomRadius;
    bool iconIsInRoom;
    handleState HandleState;

    // Start is called before the first frame update
    void Start()
    {
        roomRadius = 1.5f;
        HandleState = GameObject.Find("GameManager").GetComponent<handleState>();
    }

    // Update is called once per frame
    void Update()
    {
        distanceFromCenter = Vector3.Distance(gameObject.transform.position, roomCenter.transform.position);
        // print(distanceFromCenter);
        checkIfIconIsInRoom();
    }

    void checkIfIconIsInRoom()
    {
        if (HandleState.USBCIcon2IsInRoom == false && distanceFromCenter < roomRadius)
        {
            print("in2");
            HandleState.USBCIcon2IsInRoom = true;
            if (HandleState.deckIsConnectedToStem)
            {
                HandleState.setVinylVolume();
            }

            if (HandleState.tvIsConnectedToStem)
            {
                HandleState.setTVVolume();
            }

        }

        if (HandleState.USBCIcon2IsInRoom == true && distanceFromCenter > roomRadius)
        {
            print("oot2");
            HandleState.USBCIcon2IsInRoom = false;
            if (HandleState.deckIsConnectedToStem)
            {
                HandleState.setVinylVolume();
            }

            if (HandleState.tvIsConnectedToStem)
            {
                HandleState.setTVVolume();
            }

        }
    }


}
