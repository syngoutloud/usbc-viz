﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class handleButton : MonoBehaviour
{

    //handleTransition HandleTransition;
    //handleSettingsInfoPanel HandleSettingsInfoPanel;
    //handleStateHardball HandleStateHardball;
    //handleStateJourney HandleStateJourney;

    Vector3 startingScale;
    float pressScale = 0.95f;
    float pressDuration = 0.05f;
    float releaseDuration = 0.25f;
    //float transitionOutDelay = 0.25f;
    //float changeSceneDelay = 1f;

    //AudioSource buttonDown;
    //AudioSource buttonUp;

    //Scene currentScene;

    handleState HandleState;

    public GameObject currentlyReleasedObject;
    public GameObject currentlyClickedObject;

    




    // Use this for initialization
    void Start()
    {
        currentlyReleasedObject = null;
        currentlyClickedObject = null;

        HandleState = GameObject.Find("GameManager").GetComponent<handleState>();

        
        GetComponent<ReleaseGesture>().StateChanged += releaseHandler;

        GetComponent<PressGesture>().StateChanged += pressHandler;




    }

    void getStartingScale()
    {
        startingScale = transform.localScale;
    }



    private void releaseHandler(object sender, GestureStateChangeEventArgs e)
    {

        ReleaseGesture gesture = sender as ReleaseGesture;

        if (e.State == Gesture.GestureState.Ended)
        {
           // print("button released!");


            if (gesture.gameObject.tag == "vinylStartStop")
            {
                HandleState.toggleVinylPlayback();
                
            }

            if (gesture.gameObject.tag == "tvStartStop")
            {
                HandleState.toggleTVPlayback();

            }

            if (gesture.gameObject.tag == "deviceConnector")
            {
                //print("released " + HandleState.lastConnectorHoveredOver);

                currentlyReleasedObject = HandleState.lastConnectorHoveredOver;

                //print("line ends: " + HandleState.lastConnectorHoveredOver);

               HandleState.DrawConnectorLine(currentlyClickedObject.transform.position, currentlyReleasedObject.transform.position);

                setConnections();


            }

            if (gesture.gameObject.name == "lineDeleteButton(Clone)")
            {
                print("released delete");

                currentlyClickedObject = gameObject;
                currentlyReleasedObject = gameObject;

                setConnections();

                Destroy(gesture.gameObject.transform.parent.gameObject);

            }

            

        }

    }



    void setConnections()
    {

        //deck to cell

        if (currentlyClickedObject.name == "deviceConnectorUSBCDeck")
        {
            if (currentlyReleasedObject.name == "deviceConnectorUSBCCellBase")
            {
                HandleState.deckIsConnectedToStem = false;
                HandleState.deckIsConnectedToBase = true;
                HandleState.alertConnected.PlayOneShot(HandleState.alertConnected.clip);

            }
            else if (currentlyReleasedObject.name == "deviceConnectorUSBCCellStem")
            {
                HandleState.deckIsConnectedToStem = true;
                HandleState.deckIsConnectedToBase = false;
                HandleState.alertConnected.PlayOneShot(HandleState.alertConnected.clip);

            }
        }

        //tv to cell

        if (currentlyClickedObject.name == "deviceConnectorTV")
        {
            if (currentlyReleasedObject.name == "deviceConnectorUSBCCellBase")
            {
                HandleState.tvIsConnectedToStem = false;
                HandleState.tvIsConnectedToBase = true;
                HandleState.alertConnected.PlayOneShot(HandleState.alertConnected.clip);

            }
            else if (currentlyReleasedObject.name == "deviceConnectorUSBCCellStem")
            {
                HandleState.tvIsConnectedToStem = true;
                HandleState.tvIsConnectedToBase = false;
                HandleState.alertConnected.PlayOneShot(HandleState.alertConnected.clip);

            }
        }

        //cell base to deck/tv

        if (currentlyClickedObject.name == "deviceConnectorUSBCCellBase")
        {
            if (currentlyReleasedObject.name == "deviceConnectorUSBCDeck")
            {
                HandleState.deckIsConnectedToBase = true;
                HandleState.tvIsConnectedToBase = false;
                HandleState.alertConnected.PlayOneShot(HandleState.alertConnected.clip);

            }
            else if (currentlyReleasedObject.name == "deviceConnectorTV")
            {
                HandleState.deckIsConnectedToBase = false;
                HandleState.tvIsConnectedToBase = true;
                HandleState.alertConnected.PlayOneShot(HandleState.alertConnected.clip);
            }
        }

        //cell stem to deck/tv

        if (currentlyClickedObject.name == "deviceConnectorUSBCCellStem")
        {
            if (currentlyReleasedObject.name == "deviceConnectorUSBCDeck")
            {
                HandleState.deckIsConnectedToStem = true;
                HandleState.tvIsConnectedToStem = false;
                HandleState.alertConnected.PlayOneShot(HandleState.alertConnected.clip);

            }
            else if (currentlyReleasedObject.name == "deviceConnectorUSBCTV")
            {
                HandleState.deckIsConnectedToStem = false;
                HandleState.tvIsConnectedToStem = true;
                HandleState.alertConnected.PlayOneShot(HandleState.alertConnected.clip);
            }
        }

        //connection delete buttons

        if (currentlyReleasedObject.tag == "deleteButtonDeckToStem")
        {
            HandleState.deckIsConnectedToStem = false;
            HandleState.alertDisconnected.PlayOneShot(HandleState.alertDisconnected.clip);
            HandleState.setUSBC2State();
        }

        if (currentlyReleasedObject.tag == "deleteButtonDeckToBase")
        {
            HandleState.deckIsConnectedToBase = false;
            HandleState.alertDisconnected.PlayOneShot(HandleState.alertDisconnected.clip);
            HandleState.setUSBC1State();
        }

        if (currentlyReleasedObject.tag == "deleteButtonTVToStem")
        {
            HandleState.tvIsConnectedToStem = false;
            HandleState.alertDisconnected.PlayOneShot(HandleState.alertDisconnected.clip);
            HandleState.setUSBC2State();
        }

        if (currentlyReleasedObject.tag == "deleteButtonTVToBase")
        {
            print("deleteTVToBase");
            HandleState.tvIsConnectedToBase = false;
            HandleState.alertDisconnected.PlayOneShot(HandleState.alertDisconnected.clip);
            HandleState.setUSBC1State();
        }

        HandleState.setUSBC1State();
        HandleState.setUSBC2State();


        print("------");
        print("deckIsConnectedToBase = " + HandleState.deckIsConnectedToBase);
        print("deckIsConnectedToStem = " + HandleState.deckIsConnectedToStem);
        print("tvIsConnectedToStem = " + HandleState.tvIsConnectedToStem);
        print("tvIsConnectedToBase = " + HandleState.tvIsConnectedToBase);

    }



    void checkConnectionState()
    {
        GameObject[] connectedConnectors = GameObject.FindGameObjectsWithTag("deviceConnectorConnected");

        foreach (GameObject connector in connectedConnectors) {



        }
    }

    

    private void pressHandler(object sender, GestureStateChangeEventArgs e)
    {

        PressGesture gesture = sender as PressGesture;

        if (e.State == Gesture.GestureState.Ended)
        {
            // print("button pressed!");

            if (gesture.gameObject.tag == "deviceConnector")
            {
                print("clicked " + HandleState.lastConnectorHoveredOver);

                currentlyClickedObject = HandleState.lastConnectorHoveredOver;

                print("line starts: " + HandleState.lastConnectorHoveredOver);
            }


        }

    }

}



