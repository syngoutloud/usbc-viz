﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures;
using DG.Tweening;

public class handleCableConnectorGestures : MonoBehaviour
{

    handleState HandleState;
    GameObject[] allConnectorPoints;
    GameObject closestPoint;
    bool isPressed;
    float distanceToClosestPointForSnapping;
    float snapThresholdDistance;
    Vector3 connectorStartingScale;


    // Start is called before the first frame update
    void Start()
    {

        isPressed = false;
        snapThresholdDistance = 1f;
        connectorStartingScale = gameObject.transform.localScale;
        HandleState = GameObject.Find("GameManager").GetComponent<handleState>();
        GetComponent<ReleaseGesture>().StateChanged += releaseHandler;
        GetComponent<PressGesture>().StateChanged += pressHandler;
    }

    // Update is called once per frame
    void Update()
    {
        if (isPressed == true)
        {
            findNearestPoint();
        }

    }

    private void releaseHandler(object sender, GestureStateChangeEventArgs e)
    {

        ReleaseGesture gesture = sender as ReleaseGesture;

        if (e.State == Gesture.GestureState.Ended)
        {
           // print("connector released!");
            isPressed = false;

            if (distanceToClosestPointForSnapping < snapThresholdDistance)
            {
                snapToNearestPoint();
            }




            HandleState.setUSBC1State();
            HandleState.setUSBC2State();


        }



    }





    private void pressHandler(object sender, GestureStateChangeEventArgs e)
    {

        PressGesture gesture = sender as PressGesture;

        if (e.State == Gesture.GestureState.Ended)
        {
            //print("connector pressed!");
            isPressed = true;

        }

    }

    public void findNearestPoint()
    {

        // //create an list of all  sources
        HandleState.allDeviceConnectors = GameObject.FindGameObjectsWithTag("deviceConnector");
        allConnectorPoints = HandleState.allDeviceConnectors;


        float distanceToClosestPoint = Mathf.Infinity;
        closestPoint = null;

        foreach (GameObject point in allConnectorPoints)
        {
            float distanceToPoint = (point.transform.position - this.transform.position).sqrMagnitude;
            if (distanceToPoint < distanceToClosestPoint)
            {
                distanceToClosestPoint = distanceToPoint;
                closestPoint = point;
            }
        }

        //Debug.DrawLine(this.transform.position, closestPoint.transform.position);

        distanceToClosestPointForSnapping = Vector3.Distance(this.transform.position, closestPoint.transform.position);
        //print("d = " + distanceToClosestPoint);
        if (distanceToClosestPointForSnapping < snapThresholdDistance)
        {
            gameObject.transform.DOScale(connectorStartingScale * 1.5f, 0.1f).SetEase(Ease.OutBack);
        }
        else
        {
            gameObject.transform.DOScale(connectorStartingScale, 0.1f);
        }

    }



    public void snapToNearestPoint()
    {
        gameObject.transform.DOMove(closestPoint.transform.position, 0.2f).SetEase(Ease.OutBack);
        closestPoint.tag = "deviceConnectorConnected";


    }

}
