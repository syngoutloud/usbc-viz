﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class handleObjectGestures : MonoBehaviour
{

    public GameObject roomCenter;
    float distanceFromCenter;
    float roomRadius;
    bool iconIsInRoom;
    handleState HandleState;

    // Start is called before the first frame update
    void Start()
    {
        roomRadius = 1.5f;
        HandleState = GameObject.Find("GameManager").GetComponent<handleState>();
    }

    // Update is called once per frame
    void Update()
    {
        distanceFromCenter = Vector3.Distance(gameObject.transform.position, roomCenter.transform.position);
        // print(distanceFromCenter);
        checkIfIconIsInRoom();
    }

    void checkIfIconIsInRoom()
    {
        if (HandleState.USBCIcon1IsInRoom == false && distanceFromCenter < roomRadius)
        {
            //print("in");
            HandleState.USBCIcon1IsInRoom = true;

            if (HandleState.deckIsConnectedToBase)
            {
                HandleState.setVinylVolume();
            }

            if (HandleState.tvIsConnectedToBase)
            {
                HandleState.setTVVolume();
            }


        }

        if (HandleState.USBCIcon1IsInRoom == true && distanceFromCenter > roomRadius)
        {
           // print("oot");
            HandleState.USBCIcon1IsInRoom = false;
            if (HandleState.deckIsConnectedToBase)
            {
                HandleState.setVinylVolume();
            }

            if (HandleState.tvIsConnectedToBase)
            {
                HandleState.setTVVolume();
            }

        }
    }


}
