﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Video;

public class handleState : MonoBehaviour
{
    public GameObject vinylSpinner;
    public AudioSource vinylAudio;
    bool vinylIsPlaying;
    public GameObject vinylPlayIcon;
    public GameObject vinylPauseIcon;

    //public AudioSource TVAudio;
    bool TVIsPlaying;
    public GameObject TVPlayIcon;
    public GameObject TVPauseIcon;
    public VideoPlayer movie;


    public GameObject[] allDeviceConnectors;
    public GameObject[] allCableConnectors;


    //public bool cableUSBC1IsConnectedToCellBase;
    //public bool cableUSBC1IsConnectedToCellStem;
    //public bool cableUSBC1IsConnectedToDeck;
    //public bool cableUSBC1IsConnectedToTV;

    //public bool cableUSBC2IsConnectedToCellBase;
    //public bool cableUSBC2IsConnectedToCellStem;
    //public bool cableUSBC2IsConnectedToDeck;
    //public bool cableUSBC2IsConnectedToTV;

    public int USBC1State;
    public int USBC2State;

    public bool USBCIcon1IsInRoom;
    public bool USBCIcon2IsInRoom;

    public GameObject appObjectIconBase;
    public GameObject appObjectIconBasePaused;
    public GameObject appObjectBkgBase;
    public GameObject appObjectIconStem;
    public GameObject appObjectIconStemPaused;
    public GameObject appObjectBkgStem;

    public Color appObjectColorActive;
    public Color appObjectColorInactive;
    Vector3 appObjectStartingScale;

    public bool connectorLineDrawingIsPossible;
    public GameObject lastConnectorHoveredOver;

    public bool mouseIsPressed;
    public bool lineIsBeingDrawn;

    public GameObject[] allConnectorLines;
    public int maxNumberOfLines;

    public Material lineMaterial;

    public GameObject lineStartObject;
    public GameObject lineEndObject;

    public Transform terminalConnectorPrefab;

    public Transform lineDeleteButtonPrefab;

    public bool deckIsConnectedToStem;
    public bool deckIsConnectedToBase;
    public bool tvIsConnectedToStem;
    public bool tvIsConnectedToBase;

    GameObject currentLineDeleteButton;

    public AudioSource alertConnected;
    public AudioSource alertDisconnected;
    public AudioSource alertConnectedButSilent;


    // Start is called before the first frame update
    void Start()
    {
        appObjectStartingScale = appObjectBkgBase.transform.localScale;
        allCableConnectors = GameObject.FindGameObjectsWithTag("cableConnector");

        mouseIsPressed = false;
        USBCIcon1IsInRoom = false;
        vinylIsPlaying = true;
        TVIsPlaying = true;
        connectorLineDrawingIsPossible = false;
        maxNumberOfLines = 2;
        USBC1State = 0;
        USBC2State = 0;
        setIcon1State();
        setIcon2State();
        displayVinylPlayback();
        displayTVPlayback();

        appObjectIconStem.transform.localScale = new Vector3(0, 0, 0);
        appObjectIconBase.transform.localScale = new Vector3(0, 0, 0);

    }

    public void DrawConnectorLine(Vector3 start, Vector3 end)
    {
        allConnectorLines = GameObject.FindGameObjectsWithTag("connectorLine");

        if (allConnectorLines.Length < maxNumberOfLines && (start != end))
        {
            GameObject myLine = new GameObject();
            myLine.tag = "connectorLine";
            myLine.name = "connectorLine" + allConnectorLines.Length;
            myLine.transform.position = start;
            myLine.AddComponent<LineRenderer>();
            LineRenderer lr = myLine.GetComponent<LineRenderer>();
            lr.material = lineMaterial;
            lr.numCapVertices = 5;
            lr.alignment = LineAlignment.TransformZ;
            lr.useWorldSpace = true;
            
            lr.startColor = appObjectColorActive;
            lr.endColor = appObjectColorActive;

            lr.startWidth = 0.03f;
            lr.endWidth = 0.03f;

            lr.SetPosition(0, start);
            lr.SetPosition(1, end);

            //print("startPos " + start);
            //print("endPos" + end);

            var connectorStart = Instantiate(terminalConnectorPrefab, start, Quaternion.identity);
            connectorStart.transform.parent = myLine.transform;

            var connectorEnd = Instantiate(terminalConnectorPrefab, end, Quaternion.identity);
            connectorEnd.transform.parent = myLine.transform;

            var lineDeleteButton = Instantiate(lineDeleteButtonPrefab, (start + end) / 2, Quaternion.identity);
            lineDeleteButton.transform.parent = myLine.transform;
            

            //delay to wait for mouse up, then set delete button tag depending on what the connection is
            StartCoroutine(setDeleteButtonTag(lineDeleteButton.gameObject, 0.2f));

            

        }

        allConnectorLines = GameObject.FindGameObjectsWithTag("connectorLine");
        //there can only ever be two connector lines - you have to delete one before you can redraw
        //print(allConnectorLines.Length);
        //Destroy(myLine, duration);
    }

    IEnumerator setDeleteButtonTag(GameObject button, float delayTime)
    {
        print("called setDeleteButtonTag");

        yield return new WaitForSeconds(delayTime);

        ////BUGGG - if we connect tv to base WHILE deckIsConnectedToStem
        ///
        //we need a 'lastConnectionMade' int

        print(lastConnectorHoveredOver);

        switch (lastConnectorHoveredOver.name)
        {
            case "deviceConnectorTV":

                if (tvIsConnectedToStem == true)
                {
                    button.tag = "deleteButtonTVToStem";

                } else if (tvIsConnectedToBase == true)
                {
                    button.tag = "deleteButtonTVToBase";
                }

                break;

            case "deviceConnectorUSBCDeck":

                if (deckIsConnectedToBase == true)
                {
                    button.tag = "deleteButtonDeckToBase";
                }
                else if (deckIsConnectedToStem == true)
                {
                    button.tag = "deleteButtonDeckToStem";
                }

                break;

            case "deviceConnectorUSBCCellStem":

                if (tvIsConnectedToStem == true)
                {
                    button.tag = "deleteButtonTVToStem";
                }
                else if (deckIsConnectedToStem == true)
                {
                    button.tag = "deleteButtonDeckToStem";
                }
                break;

            case "deviceConnectorUSBCCellBase":
                if (tvIsConnectedToBase == true)
                {
                    button.tag = "deleteButtonTVToBase";
                } else if (deckIsConnectedToBase == true)
                {
                    button.tag = "deleteButtonDeckToBase";
                }
                break;
            default:
                break;
      
        }

        print("tag = " + button.tag);

        setUSBC1State();
        setUSBC2State();

    }

   

    public void setUSBC1State() //base
    {
        //0 = disconnected
        //1 = connected but silent
        //2 = connected and making sound

        if (deckIsConnectedToBase == true || tvIsConnectedToBase == true)
        {
            if (deckIsConnectedToBase == true)
            {
                if (vinylIsPlaying == true)
                {
                    USBC1State = 2;
                }
                else
                {
                    USBC1State = 1;
                }
            }
            else if (tvIsConnectedToBase == true)
            {
                if (TVIsPlaying == true)
                {
                    USBC1State = 2;
                }
                else
                {
                    USBC1State = 1;
                }
            }
        }
        else
        {
            USBC1State = 0;
        }

        setIcon1State();
        setVinylVolume();
        setTVVolume();

    }

    public void setUSBC2State() //stem
    {

        //0 = disconnected
        //1 = connected but silent
        //2 = connected and making sound

        if (deckIsConnectedToStem == true || tvIsConnectedToStem == true)
        {
            if (deckIsConnectedToStem == true)
            {
                if (vinylIsPlaying == true)
                {
                    USBC2State = 2;
                }
                else
                {
                    USBC2State = 1;
                }
            }
            else if (tvIsConnectedToStem == true)
            {
                if (TVIsPlaying == true)
                {
                    USBC2State = 2;
                }
                else
                {
                    USBC2State = 1;
                }
            }
        }
        else
        {
            USBC2State = 0;
        }

        setIcon2State();
        setVinylVolume();
        setTVVolume();
    }


    public void setIcon1State()
    {
        
        switch (USBC1State)
        {
            case 0: //disconnected
                appObjectBkgBase.transform.DOScale(new Vector3(0, 0, 0), 0.2f);
                appObjectBkgBase.GetComponent<SpriteRenderer>().DOColor(appObjectColorInactive, 0.3f);
                break;
            case 1: //connected but silent
                appObjectIconBase.transform.DOScale(new Vector3(0.1f, 0.1f, 0.1f), 0.2f);
                appObjectBkgBase.transform.DOScale(appObjectStartingScale, 0.2f);
                appObjectBkgBase.GetComponent<SpriteRenderer>().DOColor(appObjectColorInactive, 0.3f);
                break;
            case 2: //connected and making sound
                appObjectIconBase.transform.DOScale(new Vector3(0.1f, 0.1f, 0.1f), 0.2f);
                appObjectBkgBase.transform.DOScale(appObjectStartingScale, 0.2f);
                appObjectIconBase.GetComponent<SpriteRenderer>().DOFade(1f, 0.3f);
                appObjectBkgBase.GetComponent<SpriteRenderer>().DOColor(appObjectColorActive, 0.3f);
                break;
            default:
                break;

        }
    }

    

    public void setIcon2State()
    {
  
        switch (USBC2State)
        {
            case 0: //disconnected
                appObjectBkgStem.transform.DOScale(new Vector3(0, 0, 0), 0.2f);
                appObjectBkgStem.GetComponent<SpriteRenderer>().DOColor(appObjectColorInactive, 0.3f);
                break;
            case 1: //connected but silent
                appObjectBkgStem.transform.DOScale(appObjectStartingScale, 0.2f);
                appObjectIconStem.transform.DOScale(new Vector3(0.1f, 0.1f, 0.1f), 0.2f);
                appObjectBkgStem.GetComponent<SpriteRenderer>().DOColor(appObjectColorInactive, 0.3f);
                break;
            case 2: //connected and making sound
                appObjectBkgStem.transform.DOScale(appObjectStartingScale, 0.2f);
                appObjectIconStem.transform.DOScale(new Vector3(0.1f, 0.1f, 0.1f), 0.2f);
                appObjectBkgStem.GetComponent<SpriteRenderer>().DOColor(appObjectColorActive, 0.3f);
                break;
            default:
                break;

        }
    }


    public void toggleVinylPlayback()
    {
        //print("called vinylStartStop");

        if (vinylIsPlaying == false)
        {
            vinylIsPlaying = true;
        }
        else
        {
            vinylIsPlaying = false;
        }

        displayVinylPlayback();
        setUSBC1State();
        setUSBC2State();

    }

    public void toggleTVPlayback()
    {
       // print("called toggleTVPlayback");

        if (TVIsPlaying == false)
        {
            TVIsPlaying = true;
        }
        else
        {
            TVIsPlaying = false;
        }

        displayTVPlayback();
        setUSBC1State();
        setUSBC2State();

    }

    void displayTVPlayback()
    {
        if (TVIsPlaying == true)
        {
            movie.Play();
            //TVAudio.DOFade(0, 0.5f);
            TVPauseIcon.SetActive(true);
            TVPlayIcon.SetActive(false);
        }
        else
        {
            movie.Pause();
            //TVAudio.DOFade(1, 0.5f);
            TVPauseIcon.SetActive(false);
            TVPlayIcon.SetActive(true);
        }
    }



    void displayVinylPlayback()
    {
        if (vinylIsPlaying == true)
        {
            vinylSpinner.GetComponent<spinGeneric>().enabled = true;
            vinylAudio.DOPitch(1, 0.5f);
            vinylPlayIcon.SetActive(false);
            vinylPauseIcon.SetActive(true);
            vinylPlayIcon.GetComponent<SpriteRenderer>().DOFade(1, 0.2f);
        }
        else
        {
            vinylSpinner.GetComponent<spinGeneric>().enabled = false;
            vinylAudio.DOPitch(0, 0.5f);
            vinylPlayIcon.SetActive(true);
            vinylPauseIcon.SetActive(false);
        }
    }

    public void setVinylVolume()
    {

        if (deckIsConnectedToBase == true)
        {
            //print("deck is connected to base");

            if (USBCIcon1IsInRoom == true)
            {
                vinylAudio.DOFade(1, 0.5f);
            }
            else
            {
                vinylAudio.DOFade(0, 0.5f);
            }
        }
        else if (deckIsConnectedToStem == true)
        {
            //print("deck is connected to stem");

            if (USBCIcon2IsInRoom == true)
            {
                // print("yeahhh");
                vinylAudio.DOFade(1, 0.5f);
            }
            else
            {
                vinylAudio.DOFade(0, 0.5f);
            }
        }
        else
        {
            vinylAudio.DOFade(0, 0.5f);
        }

    }

    public void setTVVolume()
    {
        float volMax = 0.4f;
       

        if (tvIsConnectedToBase == true)
        {
            //print("deck is connected to base");

            if (USBCIcon1IsInRoom == true)
            {
                movie.SetDirectAudioVolume(0, volMax);
            }
            else
            {
                movie.SetDirectAudioVolume(0, 0);
            }
        }
        else if (tvIsConnectedToStem == true)
        {
            print("tv is connected to stem");

            if (USBCIcon2IsInRoom == true)
            {
                // print("yeahhh");
                movie.SetDirectAudioVolume(0, volMax);
            }
            else
            {
                movie.SetDirectAudioVolume(0, 0);
            }
        }
        else
        {
            movie.SetDirectAudioVolume(0, 0);
        }

    }


}
