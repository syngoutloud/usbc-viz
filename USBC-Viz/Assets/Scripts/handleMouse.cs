﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shapes2D;

public class handleMouse : MonoBehaviour
{
    Vector3 worldMousePosition;
    handleState HandleState;

    public Material lineMaterial;

    public Color lineColor;

    public Transform terminalConnectorPrefab;

    GameObject connectorStartPrefab;

    Vector3 lineOrigin;
    

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<SpriteRenderer>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        worldMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector3(worldMousePosition.x, worldMousePosition.y, 2.5f);
        HandleState = GameObject.Find("GameManager").GetComponent<handleState>();

        if (Input.GetMouseButtonDown(0)) {
            //Debug.Log("mouse pressed");
            HandleState.mouseIsPressed = true;

            if (HandleState.lastConnectorHoveredOver.tag == "deviceConnector")
            {
                var connectorStart = Instantiate(terminalConnectorPrefab, HandleState.lastConnectorHoveredOver.transform.position, Quaternion.identity);
                connectorStartPrefab = connectorStart.gameObject;
            }

        }

        if (Input.GetMouseButtonUp(0))
        {
            // Debug.Log("mouse released");
            HandleState.mouseIsPressed = false;
            HandleState.connectorLineDrawingIsPossible = false;
            GetComponent<SpriteRenderer>().enabled = false;
            Destroy(connectorStartPrefab);
        }

        if (HandleState.mouseIsPressed == true)
        {
            if (HandleState.connectorLineDrawingIsPossible == true && (HandleState.allConnectorLines.Length < HandleState.maxNumberOfLines))
            {
                // print("we drawin");
                GetComponent<SpriteRenderer>().enabled = true;
                DrawLine(connectorStartPrefab.transform.position, worldMousePosition, lineColor);
            }
        }
        else
        {
            Invoke("resetLineIsBeingDrawn", 0.1f);
        }
    }

    void resetLineIsBeingDrawn()
    {
        HandleState.lineIsBeingDrawn = false;
    }

    private void OnTriggerEnter(Collider other)
    {

       // print("mouse hit " + other.name);
        other.GetComponent<Shape>().settings.innerCutout = new Vector2(0.7f, 0.7f);

        if (other.tag == "deviceConnector")
        {
            //print("we can draw");
            HandleState.connectorLineDrawingIsPossible = true;
            
        }

        



    }

    private void OnTriggerExit(Collider other)
    {
       // print("mouse exited " + other.name);
        other.GetComponent<Shape>().settings.innerCutout = new Vector2(0.8f, 0.8f);

        if (HandleState.mouseIsPressed == false)
        {
            HandleState.connectorLineDrawingIsPossible = false;
        }
        
    }

    void DrawLine(Vector3 start, Vector3 end, Color color, float duration = 0.04f)
    {
        HandleState.lineIsBeingDrawn = true;

        GameObject myLine = new GameObject();
        myLine.transform.position = start;
        myLine.AddComponent<LineRenderer>();
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.material = lineMaterial;
        lr.numCapVertices = 5;
        lr.alignment = LineAlignment.TransformZ;

        lr.startColor = color;
        lr.endColor = color;

        lr.startWidth = 0.03f;
        lr.endWidth = 0.03f;

        lr.SetPosition(0, start);
        lr.SetPosition(1, end);

        Destroy(myLine, duration);
    }

}
