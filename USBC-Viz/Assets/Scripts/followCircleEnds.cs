﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class followCircleEnds : MonoBehaviour
{

    Vector3 lineStart;
    Vector3 lineEnd;
    public GameObject connectorStart;
    public GameObject connectorEnd;
    public GameObject labelCable;

    Vector3 labelMidpoint;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        lineStart = connectorStart.transform.position;
        lineEnd = connectorEnd.transform.position;

        GetComponent<LineRenderer>().SetPosition(0, lineStart);
        GetComponent<LineRenderer>().SetPosition(1, lineEnd);

        labelMidpoint = (lineStart + lineEnd) / 2;

        labelCable.transform.position = labelMidpoint;
        
    }
}
